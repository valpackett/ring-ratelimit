(defproject ring-ratelimit "0.2.3"
  :description "Rate limit middleware for Ring"
  :url "https://codeberg.org/valpackett/ring-ratelimit"
  :license {:name "WTFPL"
            :url "http://www.wtfpl.net/about/"}
  :dependencies [[org.clojure/clojure "1.7.0"]]
  :profiles {:dev {:dependencies [[midje "1.10.9"]
                                  [ring/ring-core "1.10.0"]
                                  [ring-mock "0.1.5"]
                                  [http-kit "2.6.0"]
                                  [compojure "1.7.0"]
                                  [com.taoensso/carmine "3.2.0"]
                                  [com.cemerick/friend "0.2.3"]
                                  ; https://github.com/ptaoussanis/carmine/issues/136#issuecomment-113450969
                                  ]}}
  :plugins [[lein-midje "3.0.0"]]
  :aliases {"test" ["midje" "ring.middleware.ratelimit-test"]}
  :bootclasspath true)
